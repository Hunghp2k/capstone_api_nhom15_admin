
const BASE_URL = 'https://63dd2128367aa5a7a4099979.mockapi.io/capstone';

function fetchQLSPList() {
    axios({
        url: BASE_URL,
        method: "GET",
    })

        .then(function (res) {
            renderDanhSachSP(res.data);

        })

        .catch(function (err) {
            console.log(err)
        });
}

fetchQLSPList();

function themSP() {
    var isValid = validation();
    let data = layThongTinTuFrom();
    if (isValid) {
        axios({
            url: BASE_URL,
            method: "POST",
            data: data,
        })

            .then(function (res) {
                console.log(res)
                fetchQLSPList();
                document.querySelector("#myModal .close").click();
            })

            .catch(function (err) {
                console.log(err)
            });
    } else {
        return;
    }
}

document.getElementById("btnThemSP").addEventListener("click", function () {
    var footerEle = document.querySelector(".modal-footer");
    footerEle.innerHTML = `
        <button onclick="themSP()" class="btn btn-success">Thêm sản phẩm</button>
    `;
});

function xoaSP(id) {

    axios({
        url: `${BASE_URL}/${id}`,
        method: "DELETE",
    })

        .then(function (res) {
            console.log("🚀 ~ file: script.js:56 ~ .then ~ res", res)
            fetchQLSPList();
        })

        .catch(function (err) {
            console.log("🚀 ~ file: script.js:61 ~ xoaSP ~ err", err)
        });
}

function suaSP(id) {
    axios({
        url: `${BASE_URL}/${id}`,
        method: "GET",
    })

        .then(function (res) {
            console.log("🚀 ~ file: script.js:72 ~ .then ~ res", res)
            showThongTinTuForm(res.data)
            $('#myModal').modal('show');
            var show = document.querySelector(".modal-footer");
            show.innerHTML = `<button onclick="capNhatSP()" class="btn btn-secondary">Update</button>`
        })

        .catch(function (err) {
            console.log("🚀 ~ file: script.js:61 ~ xoaSP ~ err", err)
        });
}

function capNhatSP() {
    let data = layThongTinTuFrom();

    let isValid = validation();

    if (isValid) {
        axios({
            url: `${BASE_URL}/${data.id}`,
            method: "PUT",
            data: data,
        })

            .then(function (res) {
                console.log("🚀 ~ file: script.js:93 ~ .then ~ res", res)
                fetchQLSPList();
                document.querySelector("#myModal. close").click();
            })

            .catch(function (err) {
                console.log("🚀 ~ file: script.js:99 ~ capNhatSP ~ err", err)
            });
    }

    else {
        return;
    }
}

function validation() {
    var SP = layThongTinTuFrom();

    var isValid = true;


    if (!kiemTraRong(SP.name, 0, "tbName")) {
        isValid = false;
    }

    if (!kiemTraRong(SP.price, 1, "tbGia")) {
        isValid = false;
    }

    if (!kiemTraRong(SP.screen, 2, "tbManHinh")) {
        isValid = false;
    }

    if (!kiemTraRong(SP.blackCamera, 3, "tbBlackCamera")) {
        isValid = false;
    }

    if (!kiemTraRong(SP.frontCamera, 4, "tbFrontCamera")) {
        isValid = false;
    }

    if (!kiemTraRong(SP.img, 5, "tbHinhAnh")) {
        isValid = false;
    }

    if (!kiemTraRong(SP.desc, 6, "tbMoTa")) {
        isValid = false;
    }

    if (!kiemTraChon(SP.type, "tbLSP")) {
        isValid = false;
    }

    if (!isValid) {
        return;
    }

    else {
        return SP;
    }
}

