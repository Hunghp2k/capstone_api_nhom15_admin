let error = [
    "Vui lòng nhập tên Sản Phẩm",
    "Vui lòng nhập giá",
    "Vui lòng nhập Màn hình",
    "Vui lòng nhập màu Camera",
    "Vui lòng nhập front Camera",
    "Vui lòng dán hình ảnh",
    "Vui lòng nhập mô tả",
];

function kiemTraRong(el, message, idNotify) {
    if (el == "") {
        return (
            (document.getElementById(idNotify).innerHTML = error[message]),
            (document.getElementById(idNotify).style.display = `block`),
            false
        );
    } else {
        return (
            (document.getElementById(idNotify).innerHTML = ""),
            (document.getElementById(idNotify).style.display = "none"),
            true
        );
    }
}

function kiemTraChon(value, idErr) {
    var value = value.trim();
    var a = document.getElementById(idErr);
    if (value == "0") {
        return (a.innerHTML = `Phải chọn`), (a.style.display = "block"), false;
    } else {
        return (a.innerHTML = ``), true;
    }
}


